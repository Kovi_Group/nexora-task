//
//  AppDelegate.h
//  NeXora task
//
//  Created by KOVI GROUP on 07/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

