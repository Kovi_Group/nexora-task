//
//  Model.m
//  NeXora task
//
//  Created by KOVI GROUP on 07/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//

#import "Model.h"

@implementation Model
@synthesize NeXImage;
@synthesize NeXText;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

   self.NeXImage.delegate = self;
}


@end
