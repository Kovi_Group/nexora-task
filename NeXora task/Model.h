//
//  Model.h
//  NeXora task
//
//  Created by KOVI GROUP on 07/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Model : UITableViewCell<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *NeXImage;
@property (strong, nonatomic) IBOutlet UITextView *NeXText;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@end

NS_ASSUME_NONNULL_END
