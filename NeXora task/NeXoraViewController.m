//
//  NeXoraViewController.m
//  NeXora task
//
//  Created by KOVI GROUP on 07/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//
#import "Model.h"
#import "NeXoraViewController.h"

@interface NeXoraViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSArray *NeXora;

@end

@implementation NeXoraViewController
@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self network];
}

/*=============================
 Check  the network
==============================*/
-(void)network
{
    NSURL *url = [NSURL URLWithString:@"http://jsonplaceholder.typicode.com/photo"];
    NSMutableURLRequest *headRequest = [NSMutableURLRequest requestWithURL:url];
   // headRequest.HTTPMethod = @"HEAD";
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.timeoutIntervalForResource = 10.0;
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:headRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (!error && response)
                                          {
                                               [self dataJesonFormUrl];
                                             
                                          }else{

                                              UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error Network"
                                                                                                             message:@"Check the network and try again."
                                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                                    handler:^(UIAlertAction * action) {
                                                                                                        
                                                                                                        [self network];
                                                                                                        
                                                                                                    }];
                                              
                                              [alert addAction:defaultAction];
                                              [self presentViewController:alert animated:YES completion:nil];
                                              
                                          }
                                      }];
    [dataTask resume];
    
    
    
}

/*=============================
//If network Get Data
==============================*/
-(void)dataJesonFormUrl
{
    //Data jSon url
   NSError *error;
    NSString *url_string = [NSString stringWithFormat: @"http://jsonplaceholder.typicode.com/photos"];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    //in case error server
    if (error) {
        NSLog(@"eroro");
        NSLog(@"Fatal Error");
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"An error occurred try again."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }

    //filtre get the first 10 item
    else {
        
        self.NeXora = [json subarrayWithRange:NSMakeRange(0, 10)];
        
        NSLog(@"%@",_NeXora);
        
        [self->tableView reloadData];
        
    }
    
  
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return[self.NeXora count];
}
/*=============================
//the cells have the same dimensions 150
 ==============================*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
/*=============================
//show on tableview
 ==============================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Model *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    //Get text
    NSArray *data = [self. NeXora objectAtIndex:indexPath.row];
    cell.NeXText.text = [data valueForKey:@"title"];
    
     //Get Imagen from Url
    NSString *urlString = [data valueForKey:@"thumbnailUrl"];;
    NSURLRequest *requestc = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [cell.NeXImage loadRequest:requestc];


    return cell;
}


@end
